# Ibili

> Community application to create travel groups

## Installation

- Install [Node.js](https://nodejs.org/en/) (recommended versions: npm 6.4.1 & node v8.14.0)  

- Install [NativeScript-Vue](https://nativescript-vue.org/en/docs/getting-started/installation/)  


## Usage

### Install dependencies
``` bash
npm install
```

### Build, watch for changes and run the application
``` bash
tns run <platform> --bundle
```
For example on Android:
``` bash
tns run android --bundle
```

### Build for production
``` bash
tns build <platform> --bundle
```
For example on Android:
``` bash
tns build android --bundle
```

### Build, watch for changes and debug the application
``` bash
tns debug <platform> --bundle
```
For example on Android:
``` bash
tns debug android --bundle
```
