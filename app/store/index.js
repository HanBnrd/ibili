import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import * as applicationSettings from 'tns-core-modules/application-settings'


Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		auth: applicationSettings.getBoolean("auth", false),
		token: applicationSettings.getString("token"),
		img: applicationSettings.getString("img")
	},
	mutations: {
		save(state, data) {
			state.auth = data.auth;
			state.token = data.token;
			applicationSettings.setBoolean("auth", state.auth);
			applicationSettings.setString("token", state.token);
		},
		picture(state, pic) {
			state.img = pic;
			applicationSettings.setString("img", state.img);
		}
	},
	getters: {
		auth: state => state.auth,
		token: state => state.token,
		picture: state => state.img
	  }
});

Vue.prototype.$store = store;

export default store;