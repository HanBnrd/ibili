import Vue from 'nativescript-vue'
import * as applicationSettings from 'tns-core-modules/application-settings'
import FontIcon from 'nativescript-vue-fonticon'
import App from './components/App'
import Welcome from './components/Welcome'

import store from './store';

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

Vue.use(FontIcon, {
  paths: {
    fa: './assets/css/font-awesome.css'
  }
})

Vue.prototype.$store = store;

if (store.getters.auth)
  new Vue({
    store,
    appSettings: applicationSettings,
    render: h => h('frame', [h(App)])
  }).$start()
else
  new Vue({
    store,
    appSettings: applicationSettings,
    render: h => h('frame', [h(Welcome)])
  }).$start()

